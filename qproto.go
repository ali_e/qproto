package qproto

import (
	"net"
	"time"
)

type QProto struct {
	server            *net.TCPListener
	client            *net.TCPConn
	errorLogger       func(err error, args ...interface{})
	rawDataLogger     func(data []byte, args ...interface{})
	dataBytesLimit    *int
	callback          func(data []byte)
	connectionChannel chan *Peer
}

func NewClient(address string) (client *Peer, err error) {
	var c net.Conn
	c, err = net.Dial("tcp", address)
	if err != nil {
		return
	}
	client = newConnection(c.(*net.TCPConn), &QProto{})
	return
}

func NewServer(address string) (qproto *QProto, err error) {
	qproto = &QProto{}
	/*var addr *net.TCPAddr
	addr, err = net.ResolveTCPAddr("tcp", address)
	if err != nil {
		return
	}*/
	var n net.Listener
	n, err = net.Listen("tcp", address)
	if err != nil {
		return nil, err
	}
	qproto.server = n.(*net.TCPListener)
	qproto.connectionChannel = make(chan *Peer)
	return
}

func (qp *QProto) SetErrorLogger(fn func(err error, args ...interface{})) {
	qp.errorLogger = fn
	return
}

func (qp *QProto) SetDataBytesLimit(limit int) {
	qp.dataBytesLimit = &limit
	return
}

func (qp *QProto) SetRawDataLogger(fn func(data []byte, args ...interface{})) {
	qp.rawDataLogger = fn
	return
}

func (qp *QProto) Listen() (clients chan *Peer) {
	go func() {
		for true {
			connection, err := qp.server.AcceptTCP()
			if err != nil {
				qp.errorHandler(err)
				continue
			}
			connection.SetDeadline(time.Time{})
			go qp.processConnection(connection)
		}
	}()
	return qp.connectionChannel
}

func (qp *QProto) errorHandler(err error) {
	if qp.errorLogger != nil {
		qp.errorLogger(err)
	}
}

func (qp *QProto) processConnection(conn *net.TCPConn) {
	qp.connectionChannel <- newConnection(conn, qp)
	return
}
