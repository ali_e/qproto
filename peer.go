package qproto

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"net"

	qErr "gitlab.com/ali_e/qproto/err"
)

const contentLengthBytes = 4

type Peer struct {
	parent        *QProto
	connection    *net.TCPConn
	data          chan []byte
	RemoteAddress net.Addr
}

func newConnection(conn *net.TCPConn, parent *QProto) (connection *Peer) {
	connection = &Peer{connection: conn, parent: parent, data: make(chan []byte), RemoteAddress: conn.RemoteAddr()}
	return
}

func (c *Peer) Close() error {
	return c.connection.Close()
}

func (c *Peer) Write(b []byte) (total int, err error) {
	var contentLength = make([]byte, contentLengthBytes)
	binary.BigEndian.PutUint32(contentLength, uint32(len(b)))
	data := contentLength
	data = append(data, b...)
	total, err = c.connection.Write(data)
	return
}

func (c *Peer) Connection() *net.TCPConn {
	return c.connection
}

func (c *Peer) read() {
	closeConnection := false
	defer func() {
		if closeConnection {
			fmt.Println("close connection")
			c.connection.Close()
			close(c.data)
		}
	}()
	contentLength := make([]byte, contentLengthBytes)
	// bufio
	b := bufio.NewReader(c.connection)
	for true {
		totalRead, err := io.ReadFull(b, contentLength)
		if err != nil {
			closeConnection = true
			c.parent.errorHandler(err)
			return
		}
		if c.parent.rawDataLogger != nil {
			c.parent.rawDataLogger(contentLength, "content length")
		}
		if totalRead != contentLengthBytes {
			closeConnection = true
			c.parent.errorHandler(qErr.Err("Update", "Invalid Content Length", fmt.Sprintf("read bytes: %d - %X", totalRead, contentLength)))
			return
		}

		data := binary.BigEndian.Uint32(contentLength)
		if c.parent.dataBytesLimit != nil && int(data) > *c.parent.dataBytesLimit {
			closeConnection = true
			c.parent.errorHandler(qErr.Err("Update", "Data Content Length > 2048", fmt.Sprint(data)))
			return
		}
		dataBytes, err := c.processData(b, int(data))
		if err != nil {
			closeConnection = true
			c.parent.errorHandler(err)
			return
		}
		if c.parent.rawDataLogger != nil {
			c.parent.rawDataLogger(dataBytes, "data bytes")
		}
		c.data <- dataBytes
	}
}

func (c *Peer) Updates() chan []byte {
	go c.read()
	return c.data
}

func (c *Peer) processData(conn *bufio.Reader, contentLength int) (result []byte, err error) {
	if c.parent.rawDataLogger != nil {
		c.parent.rawDataLogger(nil, "processing data")
	}
	var dataBytes = make([]byte, contentLength)
	var read int
	read, err = io.ReadFull(conn, dataBytes)
	if err != nil {
		return
	}
	if c.parent.rawDataLogger != nil {
		c.parent.rawDataLogger(dataBytes, "data bytes process data")
	}
	if read != contentLength {
		err = qErr.Err("ProcessData", "Data Content Length Does not match", fmt.Sprint(read))
		return
	}
	result = dataBytes
	return
}
