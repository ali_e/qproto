package err

type Error struct {
	sector  string
	title   string
	content string
	error
}

func Err(sector, title, content string) Error {
	return Error{sector: sector, title: title, content: content}
}

func (e Error) Error() string {
	return "QProto Error: " + e.sector + ":" + e.title + " - " + e.content
}
