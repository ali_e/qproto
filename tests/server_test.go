package tests

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/ali_e/qproto"
)

func Logger(err error, args ...interface{}) {
	fmt.Println("Error:", err)
}

func ProcessData(client *qproto.Peer) {
	/*	counter := 0
		for true {
			counter++
			if counter >= 10 {
				break
			}
			client.Write([]byte("de salam " + fmt.Sprintf("%d", counter)))
			time.Sleep(time.Second * 2)
		}*/
	for bytes := range client.Updates() {
		err := ioutil.WriteFile("music2.mp3", bytes, os.ModePerm)
		if err != nil {
			fmt.Println(err)
		}
		client.Write([]byte("success"))
	}
}

func TestServer(t *testing.T) {
	qp, err := qproto.NewServer(":8000")
	if err != nil {
		fmt.Println(err)
		return
	}
	qp.SetErrorLogger(Logger)
	for client := range qp.Listen() {
		go ProcessData(client)
	}
}
