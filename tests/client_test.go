package tests

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/ali_e/qproto"
)

func TestClient(t *testing.T) {
	qp, err := qproto.NewClient("188.40.147.171:8000")
	if err != nil {
		fmt.Println(err)
		return
	}
	for update := range qp.Updates() {
		fmt.Println(string(update))
	}
	for true {
		time.Sleep(time.Second * 2)
	}
}
